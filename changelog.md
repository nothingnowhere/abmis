## Barbarian fishing

### 1.3.1

-   Updates delays in between actions to make the script a little more efficient
-   Fix script not detecting the player state while fishing correctly

---

### 1.3.0

-   Adiciona breaks para o antiban (se vc der logout é normal, geralmente no log do simba vai mostrar que o antiban ta funcionando)
-   Adiciona opção para selecionar se vc quer só dropar os peixes ou cortá-los
-   Possivelmente corrige um bug que acontecia raramente ao arrastar os peixes para o slot correto antes de cortar

---

#### 1.2.0

-   Arruma o auto updater (agora funciona de vdd!)
-   Adiciona algumas tasks simples de antiban, como olhar as skills, mudar a aba, colocar o mouse em algum player aleatório, dar uns breaks rapidos de 1-3 minutos
-   A seleção de mundo default agora suporta uma lista de mundos que o bot escolherá automaticamente (você escolhe os mundos, tem um exemplo no código)
-   Melhorias gerais na estabilidade/confiabilidade do script com mais failsafes

---

#### 1.1.3

-   Arruma o bug de upar agilidade/str enquanto pesca e ficar parado até tomar logout

---

#### 1.1.2

-   Acho que agora as 'regras de uso' estão 100% corretas

---

#### 1.1.1

-   Corrige as 'regras de uso'

---

#### 1.1.0

-   Implementado o Auto Updater para o script
-   Correções em alguns delays entre as ações para melhoria na estabilidade

